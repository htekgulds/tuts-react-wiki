var Firebase = require('firebase');
var crypto = require('crypto');

var firebase = new Firebase('https://tuts-react-wiki.firebaseio.com/');
var users = firebase.child('users');

// Hash Function
function hash(password) {
  console.log(password);
  return crypto.createHash('sha512').update(password).digest('hex');
}

// Error Function
function error(res, message) {
  console.log(message);
  return res.json({ signedIn: false, message: message });
}

var router = require('express').Router();

router.use(require('body-parser').json());
router.use(require('cookie-parser')());
router.use(require('express-session')({
  resave: false,
  saveUninitialized: true,
  secret: 'thisismysecretword'
}));

// Sign Up
router.post('/api/signup', function (req, res) {
  var username = req.body.username,
      password = req.body.password;

  if (!username || !password) {
    return error(res, 'no username or password');
  }

  users.child(username).once('value', function (snapshot) {
    if (snapshot.exists()) {
      return error(res, 'username already in use');
    }

    var userObj = {
      username: username,
      passwordHash: hash(password)
    };

    users.child(username).set(userObj);
    req.session.user = userObj;

    res.json({ signedIn: true, user: userObj });
  });
});

// Sign In
router.post('/api/signin', function (req, res) {
  var username = req.body.username,
      password = req.body.password;

  if (!username || !password) {
    return error(res, 'no username or password');
  }

  users.child(username).once('value', function (snapshot) {
    if (!snapshot.exists() || snapshot.child('passwordHash').val() !== hash(password)) {
      return error(res, 'wrong username or password');
    }

    var user = snapshot.exportVal();
    req.session.user = user;
    res.json({ signedIn: true, user: user });
  })
});

// Sign Out
router.post('/api/signout', function (req, res) {
  delete req.session.user;
  return error(res, 'you have been signed out');
});

module.exports = router;
